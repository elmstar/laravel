<?php
namespace App\Http\Controllers;
use App\Author;
use App\Book;
use Illuminate\Http\Request;

class Bibl extends Controller {
	public function messages() {
		return [
			'name.required' =>'Необходимо указать имя, при чём не пустое',
		];
	}
    public function bookList($id = 0) {
		if (!empty($_GET['sortAuthor'])) // если есть переметр фильтрации - задействуем его
		return redirect('/books/'.$_GET['sortAuthor']);
		$authors = Author::all();
		if ($id == 0) {
			$books = Book::orderBy('name')->get();
		} else {
			$allBooks = Book::orderBy('name')->whereHas('authors')->get();// до идеального варианта пока не додумался - придумал обходной
			$books = [];
			foreach ($allBooks AS $book) {
				foreach ($book->authors AS $author) {					
					if ($author->pivot->author_id == $id)
					$books[] = $book;
				}
			}
		}
		return view('bookList', ['books'=>$books, 'authors'=>$authors, 'listMode'=>$id]);
	}
	public function bookEdit(Request $request, $id = 0) {
		$authors = Author::all();
		if ($request->isMethod('get')) {
			$bookAuthors = [];	
			if ($id == 0) {
				$act = 'new';
				$book = '';				
			} else {
				$act = 'edit';
				$book = Book::findOrFail($id);
				foreach ($book->authors AS $author)
				$bookAuthors[] = $author->id;
			}					
			return view('bookEdit', ['book'=>$book, 'authors'=>$authors, 'bookAuthors'=>$bookAuthors, 'act'=>$act]);
		} else {
			// Валидация введённых данных
			$messages = [
				'name.required' =>'Необходимо указать имя, при чём не пустое'
			];
			$this->validate($request, [
				'name'=>'required|max:255',
				'publish'=>'required|integer|max:9999'
			]);
			
			if ($id == 0) {
				$book = new Book;
			} else {
				$book = Book::findOrFail($id);
				$book->authors()->detach();
			}
			$book->name = $request->name;
			$book->publish = $request->publish;
			$book->save();
			foreach ($request->request AS $key=>$elem) {
				if (preg_match('#^auth#', $key)>0) {
					$keyArr = preg_split('#^auth_#',$key);
					$authorID = $keyArr[1];					
					$book->authors()->attach($authorID);
				}
			}
			return redirect('/books');
		}
	}
	public function bookDel($id) {
		$book = Book::find($id);
		$book->authors()->detach();
		$book->delete();
		return redirect('/books');
	}
	public function authorList() {
		$authors = Author::all();		
		return view('authorList', ['authors'=>$authors]);
	}
	public function authorEdit(Request $request, $id = 0) {
		if ($request->isMethod('get')) {
			if ($id == 0) {
				$act = 'new';
				$author = '';
			} else {
				$act = 'edit';
				$author = Author::findOrFail($id);
			}
			return view('authorEdit', ['author'=>$author, 'act'=>$act]);
		} else {
			$this->validate($request, [ 'fio' =>'required|string|max:255' ]);
			if ($id == 0)
			$author = new Author;
			else
			$author = Author::findOrFail($id);
			$author->fio = $request->fio;
			$author->save();
			return redirect('/authors');
		}
	}
	public function authorDel($id) {
		$author = Author::find($id);
		$author->books()->detach();
		$author->delete();
		return redirect('/authors');
	}
}
