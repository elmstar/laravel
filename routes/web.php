<?php
Route::get('/', function () {	return view('welcome');	});
Route::get('/books/{id?}', 'Bibl@bookList');
Route::match(['get', 'post'],'/book/{id?}','Bibl@bookEdit');
Route::get('/book/{id}/del','Bibl@bookDel');
Route::get('/authors', 'Bibl@authorList');
Route::get('/author/{id}/del','Bibl@authorDel');
Route::match(['get','post'],'/author/{id?}', 'Bibl@authorEdit'); //
