@extends('tmp.bible')
@if ($act == 'new')
@section('title', 'Новая книга')
@else
@section('title', 'Редактирование книги')
@endif
@section('main')
@if ($act == 'new')
<h1>Новая книга</h1>
@else
<h1> Редактирование книги </h1>
@endif
<form method="POST" action="">
	{{ csrf_field() }}
	<table>
	<tr><td>Название:</td><td><input class="inputName" name="name" value="{{ $book->name??'' }}"></td></tr>
	<tr><td>Год издания:</td><td><input class="inputName" name="publish" value="{{ $book->publish??'' }}" ></td></tr>
	<tr><td>Автор(ы)</td><td class="authorList">
	<table >
	@foreach ($authors AS $author)	
	<tr><td>{{ $author->fio }}</td><td> <input type="checkbox" name="auth_{{ $author->id }}"
	@if (in_array($author->id, $bookAuthors,true))
	checked
	@endif
	></td></tr>
	@endforeach
	</table>
	</td></tr>
	<tr><td><input type="hidden" name="act" value="{{ $act }}"></td>
		<td><input type="submit" value=
		@if ($act == 'new')
		"Добавить"
		@else
		"Сохранить"
		@endif
		></td></tr>
	</table>
</form>
@endsection
