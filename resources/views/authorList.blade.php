@extends('tmp.bible')
@section('title', 'Список авторов')
@section('main')
<h1>Авторы</h1>
<table>
	<thead>
	<th>id</th><th>ФИО</th><th>Количество книг</th><th colspan=2>Функции</th>
	</thead>
@foreach ($authors AS $author)
<tr>
	<td>{{ $author->id }}</td>
	<td>{{ $author->fio }}</td>
	<td>{{ count($author->books) }}</td>
	<td><form action="/author/{{ $author->id }}"><input type="submit" value="Правка"></form></td>
	<td><form action="/author/{{ $author->id }}/del"><input type="submit" value="Удаление"></form></td>
</tr>
@endforeach
<tr><td></td><td><form action="/author"><input type="submit" value="Новый"></form></td><td>
	
 
</td><td colspan=2></td></tr>
</table>
@endsection
