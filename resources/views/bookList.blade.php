@extends('tmp.bible')
@section('title', 'Список книг')
@section('main')
<h1>Список книг</h1>
<table>
	<thead>
	<th>id</th><th>Название</th><th>Автор(ы)</th><th>Год издания</th>
	</thead>
@foreach ($books AS $book)
<tr>
	<td>{{ $book->id }}</td>
	<td>{{ $book->name }}</td>
	<td>
		@foreach ($book->authors AS $author)
		
		{{ $author->fio }}
		@if (!$loop->last)
		,
		@endif
		@endforeach
	</td>
	<td>{{ $book->publish }}</td>	
	<td><form action="/book/{{ $book->id }}"><input type="submit" value="Правка"></form></td>
	<td><form action="/book/{{ $book->id}}/del"><input type="submit" value="Удалить"></form></td>
</tr>
@endforeach
<tr><td></td><td><form action="/book"><input type="submit" value="Новая"></form></td><td><form action="">
	<select name="sortAuthor">{{-- выпадающее меню с сохранением выбранного режима --}}
		<option
		@if ($listMode == 0)
		selected
		@endif
		>Все</option>
		@foreach ($authors AS $author)
		<option value="{{ $author->id }}"
		@if ($author->id == $listMode)
		selected
		@endif
		>{{ $author->fio }}</option>
		@endforeach		
	</select>
	<input type="submit" value="Применить">
	</form></td></tr>
</table>

@endsection
