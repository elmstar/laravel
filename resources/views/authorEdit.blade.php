@extends('tmp.bible')
@if ($act == 'new')
@section('title', 'Новый автор')
@else
@section('title', 'Редактирование автора')
@endif
@section('main')
@if ($act == 'new')
<h1>Новый автор</h1>
@else
<h1> Редактирование автора </h1>
@endif
<form method="POST" action="">
	{{ csrf_field() }}
<table>	
<tr><td>ФИО автора:</td><td><input name="fio" value="{{ $author->fio??'' }}"</td></tr>
<tr><td><input type="hidden" name="act" value="{{ $act }}"></td><td><input type="submit"></td></tr>
</table>
</form>
@endsection
