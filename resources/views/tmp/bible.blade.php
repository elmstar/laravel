<!DOCTYPE html>
<html>
<head>
<title>@yield('title')</title>
<link rel="stylesheet" href="/main.css">
</head>
<body>
<nav>
<ul>
<li><a href="/">Стартовая</a></li>
<hr>
<li><a href="/books">Книги</a></li>
<li><a href="/authors">Авторы</a></li>
</ul>
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
</nav>
<article>
	@yield('main')
</article>
<footer>
Тестовая задача от ReCode
</footer>
</body>
</html>
